#include "hello_topic/HelloTopicPublisher.hpp"

HelloTopicPublisher::HelloTopicPublisher(ros::NodeHandle& nodeHandle) : node_handle_(nodeHandle) {

	publisher_ = node_handle_.advertise<std_msgs::String>("my_topic", 1) ;
	counter_ = 0 ;
}

void HelloTopicPublisher::update() {

	counter_++ ;

	// change the content of the message
	message_.data = "hello world " + std::to_string(counter_) ;

	// publish the message
	publisher_.publish(message_) ;

	// output the message to the screen
	ROS_INFO_STREAM("publisher: " << message_.data) ;
}

