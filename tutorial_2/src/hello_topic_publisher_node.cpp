#include <ros/ros.h>
#include "hello_topic/HelloTopicPublisher.hpp"

int main(int argc, char **argv) {

	// initialize ROS
	ros::init(argc, argv, "publisher_node") ;

	// create a node handle
	ros::NodeHandle node_handle ;

	// specify the frequency to 100HZ
	ros::Rate loopRate(100) ;

	// create an object of HelloTopicPublisher
	HelloTopicPublisher publisher(node_handle) ;

	// ros::ok() returns false when the program is finished (e.g., when you do Ctrl-C)
	while ( ros::ok() ) {

		// Do whatever the publisher should do
		publisher.update() ;

		// call all the callbacks waiting to be called
		ros::spinOnce() ;

		// sleep for any time remaining to the publish rate
		loopRate.sleep() ;
	}

	return 0 ;
}

