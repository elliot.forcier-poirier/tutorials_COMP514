#include <ros/ros.h>
#include <std_msgs/String.h>

class HelloTopicPublisher {

	public:
		/*
		 * Constructor
		 */
		HelloTopicPublisher(ros::NodeHandle& nodeHandle);

		/*
		 * A function handling necessary actions in every loop
		 */
		void update() ;

	private:
		ros::NodeHandle& node_handle_ ; // ROS node handle.
		ros::Publisher publisher_ ; 	// make a publisher
		std_msgs::String message_ ;  	// ROS topic for storing the message
		int counter_ ;
} ;

